package com.jacobcorp.GradleRestAPI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GradleRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(GradleRestApiApplication.class, args);
	}

}
