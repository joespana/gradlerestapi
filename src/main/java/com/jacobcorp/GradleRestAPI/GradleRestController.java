package com.jacobcorp.GradleRestAPI;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GradleRestController {

	private static final String template = "Hello, %s!";
	private final AtomicLong counter = new AtomicLong();
	
	@GetMapping("/greeting")
	public GreetingDTO greeting(@RequestParam(value="name", defaultValue="world")String name) {
		
		return new GreetingDTO(counter.incrementAndGet(), String.format(template, name));
		
	}
}
